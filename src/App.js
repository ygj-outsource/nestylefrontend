import { Route, Switch } from 'react-router';
import 'semantic-ui-css/semantic.min.css'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import Header from './components/Header';
import Footer from './components/Footer';
import { Container } from 'semantic-ui-react';
import Home from './pages/Home';
// import Closet from "./Closet"
// import Brand from "./Brand"
import Category from './pages/Category';
import Fitting from './pages/Fitting';
// import List from "./List";
import Hairshop from './pages/Hairshop';
import Search from "./pages/Search";
import Products from './Products';
import { ListBrand, ListCategory } from './pages/List'
import Hairsalon from './pages/Hairsalon';

function App() {
  return (
    <>
      <Switch>
        {/* <Route path="/closet" component={Closet} exact/> */}
        <Route path="/hairshop" component={Hairshop} exact/>
        <Route path="/hairsalon" component={Hairsalon} exact/>
        <Route path="/category" component={Category} exact/>
        <Route path="/fitting" component={Fitting} exact/>
        {<Route path="/list/category/:category" component={ListCategory} exact/>}
        {<Route path="/list/brand/:brand" component={ListBrand} exact/>}
        <Route path="/search/:query" component={Search} exact/>
        {/* <Route path="/brand" render={() => <Products defaultShowType="brand"/>} exact/> */}
        <Route path="/all" component={Products}/>
        <Route path="/" component={Home}/>
      </Switch>
    </>
  );
}

export default App;