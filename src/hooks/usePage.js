

function usePage(pageInfo, count)
{
    let nextVar = {
        afterCursor: pageInfo.endCursor,
        first: count
    }
    let prevVar = {
        beforeCursor: pageInfo.startCursor,
        last: count
    }
    var hasNext = pageInfo.hasNext
    var hasPrev = pageInfo.hasPrev

    return {hasNext, hasPrev, nextVar, prevVar}
}