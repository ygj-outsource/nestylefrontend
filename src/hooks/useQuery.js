import { request } from "graphql-request"
import { useEffect, useRef } from "react"
import { base_url } from "../config"
import useAsync from "./useAsync"

const endpoint = base_url + "/graphql/"
const query_cache = {

}
function useQuery(query, {cache=false, variable=null})
{
    const queryKey = query + JSON.stringify(variable)
    var d = useAsync(async () => {
        console.log("request to " + endpoint, query)
        if(cache && query_cache.hasOwnProperty(query)) {
            console.log("cache : ", query_cache[queryKey])
            return [{loading: false, data: query_cache[query], error: null}, null]
        }
        let ret
        if(variable)
            ret = await request(endpoint, query, variable)
        else
            ret = await request(endpoint, query)
        if(cache && ret[0] != null) query_cache[queryKey] = ret[0].data

        return ret
    }, [])

    return d[0] // loading, data, error
}

export default useQuery