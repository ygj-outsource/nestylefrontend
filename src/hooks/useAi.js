import axios from "axios"
import { request } from "graphql-request"
import { useMemo, useState } from "react"
import { base_url } from "../config"
import useAsync from "./useAsync"
const request_endpoint = base_url + "/api/request_ai"
const check_endpoint = base_url + "/api/check_ai/"




export default function useAi(requestType, modelImage, param)
{
    const [loading, setLoading] = useState(true)
    const [statusMessage, setStatusMessage] = useState('사진합성을 준비중입니다')
    const [result, setResult] = useState('')
    const [error, setError] = useState('')

    let get_url = `${request_endpoint}?request_type=${requestType}&model_id=${modelImage}&param=${param}`
    axios.get(get_url).then((r) => {
        if(r.result)
        {
            setLoading(false)
            setStatusMessage('사진합성이 완료되었습니다.')
            setResult(r.result)
        }else{
            setStatusMessage(JSON.stringify(r))
            const checkAi = (task_id) => {
                axios.get(check_endpoint + task_id).then((r) => {
                    if(r.result) {
                        setLoading(false)
                        setStatusMessage('사진합성이 완료되었습니다.')            
                    }else {
                        setStatusMessage(JSON.stringify(r))
                        setTimeout(() => {checkAi(task_id)}, 1000)
                    }
                })
            }
            checkAi(r.task_id)
        }
    })
    const init = () => {
        setLoading(false)
        setResult('')
        setError('')
    }
    return {loading, statusMessage, result, error, init}
}