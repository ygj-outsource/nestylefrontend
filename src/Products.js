import Slider from "react-slick";
import { useMediaQuery } from "react-responsive";
import { Container, Form, Grid, Input, Radio, Search } from 'semantic-ui-react';
import background from "./images/default.jpg"
import { useParams } from "react-router";
import { useContext, useEffect, useMemo, useState } from "react";
import useQuery from "./hooks/useQuery";
import request from "graphql-request";
import { base_url, query_url } from "./config";
import 'moment/locale/ko';
import { Button } from "semantic-ui-react"
import Moment from "react-moment";
import LoadingOverlay from "react-loading-overlay";
import { CartProvider, CartContext } from './contexts/CartContext'
import { ColorContext, ColorProvider } from "./contexts/ColorContext";
import Color from "./components/Color";
import ColorContainer from "./components/Color";

const brandQuery = `
query(
    $lastCount: Int,
    $firstCount: Int,
    $beforeCursor: String,
    $afterCursor: String,
    $query: String
) {
  result: productByBrand(query: $query) {
  
    key: brand {
      name
      id
    }
    products(
            before: $beforeCursor
            after: $afterCursor
            last: $lastCount
            first: $firstCount      
    ) {
        pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
        }
      edges {
        node {
           name
           id
          brand {
            name
            id
          }
          category {
            name
            id
          }
          image
          createDate
        }
      }
    }
  }
}
`

const categoryQuery = `
query(
    $lastCount: Int,
    $firstCount: Int,
    $beforeCursor: String,
    $afterCursor: String,
    $query: String
) {
    result: productByCategory(query: $query) {
  
    key: category {
      name
      id
    }
    products(
            before: $beforeCursor
            after: $afterCursor
            last: $lastCount
            first: $firstCount      
    ) {
        pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
        }
      edges {
        node {
           name
           id
          brand {
            name
            id
          }
          category {
            name
            id
          }
          image
          createDate
        }
      }
    }
  }
}
`

const filterQuery = `
`
function Product(props) {
    const { cart, addCart, removeCart } = useContext(CartContext)
    const isPicked = cart.find(e => e.name === props.item.name)
    // const isPicked = 0

    return (
        <div className="product_item" style={{ backgroundColor: isPicked ? "#f3f3f3" : "#ffffff" }}>
            <div className="product_image" style={{ backgroundImage: "url('" + base_url + props.item.image + "')" }} />
            <div className="product_item_text">
                <div>{props.item.name}</div>
                <div className="product_comment">
                    <div>{props.item.brand.name} · <Moment format="YYYY-MM-DD">{props.item.create || props.createDate}</Moment></div>
                    {
                        isPicked ? (
                            <Button circular color="red" onClick={() => { removeCart(props.item) }} style={{ width: 60, height: 25, padding: 0, fontSize: 13 }}>Unpick</Button>
                        ) :
                            (
                                <Button circular color="blue" onClick={() => { addCart(props.item) }} style={{ width: 60, height: 25, padding: 0, fontSize: 13 }}>Pick</Button>
                            )
                    }
                </div>
            </div>
        </div>
    )
}
function getNextPageIfExist(pageInfo, count)
{
    let nextVar = {
        afterCursor: pageInfo.endCursor,
        firstCount: count,
        hasNext: true
    }
    let prevVar = {
        beforeCursor: pageInfo.startCursor,
        lastCount: count
    }
    var hasNext = pageInfo.hasNextPage
    var hasPrev = pageInfo.hasPrevPage
    if(hasNext)
    {
        return nextVar
    }
    return {firstCount: count, hasNext: false}
}
function Products(props) {
    const pageCount = 3
    const isMobile = useMediaQuery({ 'query': "(max-width: 450px)" })
    const isTablet = useMediaQuery({ 'query': "(max-width: 1100px)" })
    // const isDesktop = useMediaQuery({ 'query': "(min-width: 1100px)" })
    const [products, setProducts] = useState({})
    const [searchFilter, setSearchFilter] = useState({
        ...props.detailQuery,
        name: props.defaultQuery ? props.defaultQuery : ""
    })
    const [showType, setShowType] = useState((props.defaultShowType || 'brand'))
    console.log('showType : ', (props.defaultShowType || 'brand'))
    const [loading, setLoading] = useState(false)
    var query = ''
    const [page, setPage] = useState(1)
    const [metaInfo, setMetaInfo] = useState({})
    const [pageInfo, setPageInfo] = useState(getNextPageIfExist({}, pageCount))
    let graphqlQuery = ''
    if(showType === 'brand') graphqlQuery = brandQuery
    if(showType === 'category') graphqlQuery = categoryQuery

    if (searchFilter.name) {
        query = "&name=" + searchFilter.name
    }
    if (searchFilter.color && searchFilter.color.clicked) {
        query = "&color=" + searchFilter.color.hexcode
    }
    if(searchFilter.category) {
        query = "&category=" + searchFilter.category
    }
    if(searchFilter.brand) {
        query = "&brand=" + searchFilter.brand
    }
    query = query.substr(1)
    var _metaInfo = {}
    const fetchData = (objects) => {
        let productObject = products
        for (var i of objects) {
            if(!productObject[i.key.name]){
                productObject[i.key.name] = []
                _metaInfo[i.key.name] = i.key
            }
            console.log("edges : ", i.products.edges)
            productObject[i.key.name] = productObject[i.key.name].concat(i.products.edges)
            if(props.pagination)
            {
                // 1개 리스트에 대한 더보기버튼만 지원
                console.log('pageinfo : ', i.products.pageInfo)
                console.log('nextPageInfo : ', getNextPageIfExist(i.products.pageInfo, pageCount))
                setPageInfo(
                    getNextPageIfExist(i.products.pageInfo, pageCount)
                )
            }
        }
        setMetaInfo(_metaInfo)
        console.log("setProduct finished : ", productObject)
        return productObject
    }
    useEffect(() => {
        let productObject = {}
        let variable = {
            ...pageInfo,
            'query': query,
        }
        let showType = props.defaultShowType || 'brand'
        setLoading(true)
        request(query_url, graphqlQuery, variable).then((r) => {
            r = fetchData(r.result)
            setProducts(r)
            setLoading(false)
        })

        console.log('productObject : ', productObject)
        console.log("effect showType : ", showType)
    }, [showType, searchFilter, props.defaultShowType, page])
    return (
        <Container className="page">
            {
                props.searchable ? (
                    <div className="search_bar">
                        <div>
                            <Form>
                                <Form.Group inline>
                                    <Form.Field>
                                        <Radio
                                            label="브랜드별"
                                            name="radioGroup"
                                            value="brand"
                                            checked={showType === 'brand'}
                                            onChange={(e, { value }) => { setShowType(value) }}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <Radio
                                            label="카테고리별"
                                            name="radioGroup"
                                            value="category"
                                            checked={showType === 'category'}
                                            onChange={(e, { value }) => { setShowType(value) }}
                                        />
                                    </Form.Field>
                                </Form.Group>

                            </Form>
                            <ColorContainer onClick={(c) => {
                                setSearchFilter({ ...searchFilter, 'color': c })
                            }} />
                        </div>
                        <div>
                            <Search onSearchChange={(e, v) => { setSearchFilter({ ...searchFilter, 'name': v.value }) }} value={searchFilter.name} placeholder="검색" />
                        </div>
                    </div>
                ) : <></>
            }

            <LoadingOverlay
                active={loading}
                spinner
                text="상품을 검색하는 중입니다"
            >
                <div style={{ minHeight: 300 }}>
                    {Object.entries(products).map(([key, value], idx) => {
                        return <>
                            <div className="label_title">
                                <h2>{key}</h2>
                                
                                {props.pagination? (<></>):(<a href={`/list/${showType}/${metaInfo[key].id}/`}>더보기</a>)}
                            </div>
                            <Grid columns={isMobile ? 1 : isTablet ? 3 : 4}>
                                {
                                    value.map((v) => {
                                        return (<Grid.Column><Product key={v.node.id} item={v.node} /></Grid.Column>)
                                    })
                                }
                            </Grid>

                            {
                                (props.pagination && pageInfo.hasNext)?(
                                    <Button color="white" icon="plus" onClick={() => {
                                        setPage(page + 1)
                                    }}>더보기</Button>
                                ):(<></>)
                            }
                        </>
                    })}
                </div>
            </LoadingOverlay>
        </Container>
    )
}


export default function ProductsWrapper(props) {
    return (
        <CartProvider>
            <ColorProvider>
                <Products {...props}/>
            </ColorProvider>
        </CartProvider>
    )
}