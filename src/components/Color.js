import { useContext, useState } from "react"
import { ColorContext } from "../contexts/ColorContext"


export function Color(props) {
    const clicked = props.clicked
    const style = {
        backgroundColor:props.color.hexcode,
        display: 'flex',
         width:'20px',
          height: '20px',
          borderRadius: '20px',
          margin: '10px',
          alignItems: 'center',
          justifyContent: 'center',
          borderColor: 'black',

        }
        if(clicked)
        {
            style.content = 'V'
            style.color = 'white'
            style.borderWidth = 2
            style.borderStyle = 'solid'
        }
    return (
        <div style={style}>
            {clicked?'V':''}
        </div>
    )
}

function ColorContainer(props)
{
    const { colors } = useContext(ColorContext)
    const style={
        'display': 'flex'
    }
    const [clicked, setClicked] = useState({
    })
    return <div style={style}>
            {
                colors.map((v) => {
                    return (
                    <div onClick={() => {
                        if(v.clicked == false)
                        {
                            setClicked(v)
                            if(props.onClick) props.onClick(v)
                            colors.map((i) => {
                                i.clicked = false
                            })
                            v.clicked = true
    
                        }else {
                            setClicked({})
                            v.clicked = false
                            if(props.onClick) props.onClick(null)
                        }
                    }}>
                        <Color color={v} 
                        clicked = {v.clicked}
                        />
                    </div>
                        )
                })
              }
              <div>{clicked.name}</div>
    </div>
    return null;
}

export default ColorContainer;