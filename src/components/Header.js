import { useState } from 'react';
import { Link } from 'react-router-dom';
import { Container, Dropdown, Input, Menu } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom'
import logo from "../images/logo.svg"

export default function Header(props) {
    const history = useHistory()
    const [query, setQuery] = useState(props.defaultQuery || "")
    const onSearchHandler = (e, {value}) => {
        setQuery(value)
    }
    const onKeydownHandler = (e) => {
        if(e.keyCode === 13) {
            search()
        }
    }
    const search = () => {
        history.push("/search/" + encodeURIComponent(query))
    }
    return (
        <div>
            <Container>
                <Menu text stackable>
                    <Menu.Item header as={Link} to="/"><span className="logo">NeStyle</span></Menu.Item>
                    <Menu.Menu position='right'>
                        <Menu.Item>
                            <Input icon='search' placeholder='의류 검색' onChange={onSearchHandler} value={query} onKeyDown={onKeydownHandler}/>
                        </Menu.Item>
                        <Menu.Item style={{marginRight: 20}} as={Link} to="/category">
                            <div className="menuItem">의상 카테고리</div>
                        </Menu.Item>
                        <Menu.Item style={{marginRight: 20}} as={Link} to="/hairshop">
                            <div className="menuItem">헤어샵</div>
                        </Menu.Item>
                        <Menu.Item style={{marginRight: 20}} as={Link} to="/hairsalon">
                            <div className="menuItem">헤어살롱</div>
                        </Menu.Item>
                        <Menu.Item style={{marginRight: 20}} as={Link} to="/fitting">
                            <div className="menuItem">피팅룸</div>
                        </Menu.Item>
                        {/* <Dropdown text='가상 피팅' item>
                            <Dropdown.Menu>
                                <Dropdown.Item as={Link} to="/brand">브랜드 룸</Dropdown.Item>
                                <Dropdown.Item as={Link} to="/fitting">피팅룸</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown> */}
                    </Menu.Menu>
                </Menu>
            </Container>
        </div>
    )
}