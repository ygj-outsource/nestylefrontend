import { Container } from "semantic-ui-react";

export default function Footer(props) {
    return (
        <footer>
            <Container>
                <div className="logo">NeStyle</div>
                <br/>
                Copyright © 2021 <b>NeStyle</b>, All Rights Reserved.
            </Container>
        </footer>
    )
}