import { useMediaQuery } from "react-responsive";
import Slider from "react-slick";
import Product from "./Product";


function Items(props) {
  
    const isMobile = useMediaQuery({ 'query': "(min-width: 450px)" })
  
    var settings = {
      dots: isMobile ? false : true,
      infinite: true,
      speed: 500,
      slidesToShow: Math.min(isMobile ? 3 : 1, products.length),
      slidesToScroll: 1,
      arrows: true,
    };
  
    return (
      <>
        <div className="label_title">
          <h3>{props.title}</h3>
        </div>
        <Slider {...settings}>
          {
            props.products.map((item, i) => {
              item['brand_name'] = item.brand.name
              return (
                <Product zzim {...item}/>
              )
            })
          }
        </Slider>
      </>
    )
  }