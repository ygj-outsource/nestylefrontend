import product_image from "../images/test.jpg"
import 'moment/locale/ko';
import { Button } from "semantic-ui-react"
import Moment from "react-moment";
import { useProduct } from "../contexts/ProductContext";
import { base_url } from "../config";

export default function Product(props) {
    const product = useProduct()
    const isPicked = -1 < Object.keys(product.cart).indexOf(props.id + "") 
    // const isPicked = 0

    return (
        <div className="product_item" style={{backgroundColor: props.isMount ? "#f3f3f3" : "#ffffff"}}>
            <div className="product_image" style={{backgroundImage: "url('" + base_url + props.image + "')"}}/>
            <div className="product_item_text">
                <div>{props.name}</div>
                <div className="product_comment">
                    <div>{props.brand_name} · <Moment format="YYYY-MM-DD">{props.create || props.createDate}</Moment></div>
                    {
                        props.zzim && isPicked ? (
                            <Button circular color="red" onClick={() => {product.unpick(props)}} style={{width: 90}}>Unpick</Button>
                        ) : props.zzim ? 
                        (
                            <Button circular color="blue" onClick={() => {product.pick(props)}} style={{width: 90}}>Pick</Button>
                        ) : (
                            <Button circular color="olive" onClick={props.onMount}>입어보기</Button>
                        )
                    }
                </div>
            </div>
        </div>
    )
}