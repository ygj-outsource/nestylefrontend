import Footer from "../../components/Footer";
import Header from "../../components/Header";
import styles from "./styles.module.css";
import background from "./images/default.jpg"
import { Container } from "semantic-ui-react";
import ProductsWrapper from "../../Products";
import { useParams } from "react-router";

export default function Search(props) {
    const { query } = useParams()

    return (
        <>
            <Header defaultQuery={query}/>
            <div className="topImage banner_small" style={{ height: 140, backgroundImage: "url('" + background + "')" }}>
                <div>
                    <Container style={{ height: "100%" }}>
                        <div className="banner_contents">
                            <div style={{ fontSize: 30 }}>"{query}" 로 검색한 내역입니다.</div>
                            <div style={{ marginTop: 10 }}>찾으시는 의상이 있으신가요?</div>
                        </div>
                    </Container>
                </div>
            </div>
            <Container>            
                <ProductsWrapper searchable defaultQuery={query}/>
            </Container>
            <Footer />
        </>
    )
}