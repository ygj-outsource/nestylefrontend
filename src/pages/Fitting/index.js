import { useContext, useEffect, useMemo, useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import { Button, Container, Dropdown, Grid, Icon } from 'semantic-ui-react';
import Product from '../../components/Product';
import { useProduct } from '../../contexts/ProductContext';
import background from "../../images/fitting.jpg"
import test_model from "../../images/model_test.jpg"
import axios from 'axios'
import LoadingOverlay from 'react-loading-overlay';
import { base_url } from '../../config';
import { useAI } from '../../contexts/AIContext';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import { CartContext } from '../../contexts/CartContext';
import request from 'graphql-request';

export default function Fitting(props) {
    const ai = useAI()
    const product = useProduct()
    const isMobile = useMediaQuery({ 'query': "(max-width: 650px)" })
    const isTablet = useMediaQuery({ 'query': "(min-width: 800px)" })
    const isDesktop = useMediaQuery({ 'query': "(min-width: 1100px)" })

    const [mount, setMount] = useState(null)
    const [image, setImage] = useState(ai.result)
    const [startLoading, setStartLoading] = useState(ai.loading)
    const [loadingText, setLoadingText] = useState(ai.statusMessage)
    const [model, setModel] = useState("")
    const [category, setCategory] = useState([])
    const [activeCategory, setActiveCategory] = useState("1")
    useMemo(() => {
        setStartLoading(ai.loading)
        setLoadingText(ai.statusMessage)
        if(ai.result)
        setImage(ai.result)
    }, [ai])
    const cart = useContext(CartContext)


    // 카테고리 목록을 보여줍니다.
    useEffect(() => {
        ai.init()
        const rows = product.category.map(item => {
            return {
                "key": item.id,
                "value": item.id,
                "text": item.name
            }
        })
        setCategory(rows)
    }, [product.category])

    // 이미지를 업로드합니다.
    const uploadImage = (event) => {
        if (event.target.files !== null) {
            const fd = new FormData();
            fd.append('file', event.target.files[0]);
            axios
                .post(base_url + '/api/one_time_upload/', fd)
                .then(res => {
                    //alert("image : " + res.data.upload_id)
                    setImage(res.data.upload_id)
                    setModel(res.data.upload_id)
                })
        }
    };
    // const checkAi = (task_id) => {
    //     (async (task_id) => {
    //         let result = await axios.get(base_url + "/api/check_ai/" + task_id)
    //         if(result.data.result) {
    //             setImage(result.data.result)
    //             setStartLoading(false)
    //             console.log("image result : http://localhost:8000/api/one_time_download?upload_id=" + result.data.result)
    //         } else {
    //             setTimeout(() => {checkAi(task_id)}, 1000)
    //         }
    //     })(task_id)
    // }
    // 옷을 착용합니다
    const onSubmit = () => {
        ai.execute('fitting', model, mount)
    }

    return (
        <>
            <Header/>
            <div className="topImage banner_small" style={{ backgroundImage: "url('" + background + "')" }}>
                <div>
                    <Container style={{ height: "100%" }}>
                        <div className="banner_contents">
                            <div>피팅룸</div>
                            <div>마음에 드는 옷을 마음껏 입어보세요!</div>
                        </div>
                    </Container>
                </div>
            </div>
            <Container className="page">
                <h2>1. 모델 사진을 선택해주세요.</h2>
                <LoadingOverlay
                    active={startLoading}
                    spinner
                    text={loadingText}
                >
                    <div className="model_image" onClick={() => { document.getElementById("image_upload").click() }}>
                        {
                            (image) ? (
                                <img src={base_url + "/api/one_time_download?upload_id=" + image}/>
                            ) : (
                                <div>
                                    사진 선택
                                    &nbsp;
                                    <Icon name="upload" />
                                </div>
                            )
                        }
                        <input type="file" style={{ display: "none" }} id="image_upload" onChange={uploadImage} />
                    </div>
                </LoadingOverlay>
                <br />
                <br />

                <h2>2. 저장한 의상을 선택해주세요.</h2>

                <Dropdown selection options={category} value={activeCategory} onChange={(e, { value }) => setActiveCategory(value)} />
                <div>
                    <Grid columns={isDesktop ? 3 : isTablet ? 2 : 2}>
                        
                        {
                            Object.keys(cart.cart).filter(key => cart.cart[key].category.id == activeCategory).map(key => {
                                // key.isn
                                const prod = cart.cart[key]
                                if (!prod) return
                                return (
                                    <Grid.Column>
                                        <Product {...prod} zzim={false} onMount={() => {setMount(prod.id)}} isMount={prod.id === mount}/>
                                    </Grid.Column>
                                )
                            })
                        }
                    </Grid>
                </div>

                <br />
                <br />

                <hr />

                <br />
                <div style={{ textAlign: "right" }}>
                    <Button color="blue" circular size="big" onClick={onSubmit}>옷 입어보기</Button>
                </div>
            </Container>
            <Footer/>
        </>
    )
}