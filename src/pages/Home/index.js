import { Container } from "semantic-ui-react";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import ProductsWrapper from "../../Products";
import background from "./images/default.jpg"
import styles from "./styles.module.css";

export default function Home(props) {
    return (
        <>
            <Header />
            <div className="topImage" style={{ backgroundImage: "url('" + background + "')" }}>
                <div>
                <Container style={{ height: "100%" }}>
                    <div className="banner_contents">
                    <div>NeStyle</div>
                    <div>어떠한 말을 적어서 한 문장으로 이 홈페이지의 특징을 드러낼 수 있을까요?</div>
                    </div>
                </Container>
                </div>
            </div>
            <ProductsWrapper defaultShowType="brand"/>
            <Footer />
        </>
    )
}