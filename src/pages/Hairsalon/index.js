import { useEffect, useMemo, useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import { Button, Container, Dropdown, Grid, Icon } from 'semantic-ui-react';
import Product from '../../components/Product';
import { useProduct } from '../../contexts/ProductContext';
import background from "../../images/salon.jpg"
import test_model from "../../images/model_test.jpg"
import axios from 'axios'
import LoadingOverlay from 'react-loading-overlay';
import { base_url } from '../../config';
import useQuery from '../../hooks/useQuery';
import { gql } from 'graphql-request';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import { useAI } from '../../contexts/AIContext';
import { ChromePicker } from 'react-color';
import { ColorPicker, useColor } from "react-color-palette";

function rgb2hsv(r,g,b) {
    let v=Math.max(r,g,b), c=v-Math.min(r,g,b);
    let h= c && ((v==r) ? (g-b)/c : ((v==g) ? 2+(b-r)/c : 4+(r-g)/c)); 
    return [60*(h<0?h+6:h), v&&c/v, v];
}

export default function Hairsalon(props) {
    const product = useProduct()

    const allColor = useQuery(gql`
        query {
            allColor {
                id
                name
                hexcode
            }
        }
    `, { cache: true })

    const isMobile = useMediaQuery({ 'query': "(max-width: 650px)" })
    const isTablet = useMediaQuery({ 'query': "(min-width: 800px)" })
    const isDesktop = useMediaQuery({ 'query': "(min-width: 1100px)" })

    const [mount, setMount] = useState(null)
    const [image, setImage] = useState("")
    const [startLoading, setStartLoading] = useState(false)
    const [loadingText, setLoadingText] = useState("사진합성을 준비중입니다.")
    const [model, setModel] = useState("")
    const [category, setCategory] = useState([])
    const [activeCategory, setActiveCategory] = useState(1)
    const [active, setActive] = useState({})
    const [customColor, setCustomColor] = useColor("hex", "#ff5959")
    const ai = useAI()
    useMemo(() => {
        setStartLoading(ai.loading)
        setLoadingText(ai.statusMessage)
        if(ai.result)
        setImage(ai.result)
    }, [ai])
        
    // 카테고리 목록을 보여줍니다.
    useEffect(() => {
        const rows = product.category.map(item => {
            return {
                "key": item.id,
                "value": item.id,
                "text": item.name
            }
        })
        console.log(rows)
        setCategory(rows)
    }, [product.category])

    // 이미지를 업로드합니다.
    const uploadImage = (event) => {
        if (event.target.files !== null) {
            const fd = new FormData();
            fd.append('file', event.target.files[0]);
            axios
                .post(base_url + '/api/one_time_upload/', fd)
                .then(res => {
                    //alert("image : " + res.data.upload_id)
                    setImage(res.data.upload_id)
                    setModel(res.data.upload_id)
                })
        }
    };

    const onSubmit = () => {
        const hexcode = customColor.hex // active.id
        ai.execute('hair_dyeing', image, hexcode.substr(1))

    }

    const colors = !allColor.data ? [] : allColor.data.allColor

    return (
        <>
            <Header/>
            <div className="topImage banner_small" style={{ backgroundImage: "url('" + background + "')" }}>
                <div>
                    <Container style={{ height: "100%" }}>
                        <div className="banner_contents">
                            <div>헤어살롱</div>
                            <div>어떤 머리든 해드립니다!</div>
                        </div>
                    </Container>
                </div>
            </div>
            <Container className="page">

                <div className="hairshop">
                    <div>
                        <h2>모델 사진을 선택해주세요.</h2>
                        <LoadingOverlay
                            active={startLoading}
                            spinner
                            text={loadingText}
                        >
                            <div className="model_image" onClick={() => { document.getElementById("image_upload").click() }}>
                                {
                                    (image) ? (
                                        <img src={base_url + "/api/one_time_download?upload_id=" + image} />
                                    ) : (
                                        <div>
                                            사진 선택
                                            &nbsp;
                                            <Icon name="upload" />
                                        </div>
                                    )
                                }
                                <input type="file" style={{ display: "none" }} id="image_upload" onChange={uploadImage} />
                            </div>
                        </LoadingOverlay>
                    </div>
                    <div>
                        <h2>스타일 선택</h2>
                        <div className="hairstyleGroup">
                            {
                                [1,2,3,4].map((item, idx) => {
                                    const id = idx
                                    const image = "http://hair.nestyle.ai/static/data/sample04.png"
                                    const title = "귀염뽀짝 헤르미온느 머리"
                                    return (
                                        <div className={"hairstyleItem " + (active === id ? "active" : "")} onClick={() => setActive(id)}>
                                            <div style={{backgroundImage: "url('" + image + "')"}}></div>
                                            <div>{title}</div>
                                        </div>
                                    )
                                })
                            }
                        </div>

                    </div>
                </div>
                <br />
                <div style={{ textAlign: "right" }}>
                    <Button color="blue" circular size="big" onClick={onSubmit}>적용하기</Button>
                </div>
            </Container>
            <Footer/>
        </>
    )
}