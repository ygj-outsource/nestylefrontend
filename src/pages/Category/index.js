import Footer from "../../components/Footer";
import Header from "../../components/Header";
import ProductsWrapper from "../../Products";
import background from "./images/closet.jpg"
import { Container } from 'semantic-ui-react';
import styles from "./styles.module.css";

export default function Category(props) {
    return (
        <>
            <Header />
            <div className="topImage banner_small" style={{ backgroundImage: "url('" + background + "')" }}>
                <div>
                <Container style={{ height: "100%" }}>
                    <div className="banner_contents">
                    <div>카테고리</div>
                    <div>찾으시는 옷들을 카테고리별로 보여드릴게요.</div>
                    </div>
                </Container>
                </div>
            </div>
            <ProductsWrapper defaultShowType="category"/>
            <Footer />
        </>
    )
}