import Footer from "../../components/Footer";
import Header from "../../components/Header";

import styles from "./styles.module.css";
import ProductsWrapper from "../../Products";
import { useParams } from "react-router";
export function ListCategory(props) {
    let r = useParams()
    return (
        <>
        
        <Header />
        <ProductsWrapper defaultShowType='category' pagination detailQuery={r}>

        </ProductsWrapper>
        <Footer />
        </>
    )
}

export function ListBrand(props) {
    let r = useParams()
    return (
        <>
        
        <Header />

<ProductsWrapper defaultShowType='brand' pagination detailQuery={r}>

</ProductsWrapper>
        <Footer />
        </>
    )

}