import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import "react-color-palette/lib/css/styles.css";

import {
  BrowserRouter as Router,
} from "react-router-dom";
import { ProductProvider } from './contexts/ProductContext';
import { AIProvider } from './contexts/AIContext';
import { ColorProvider } from './contexts/ColorContext';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <ProductProvider>
        <AIProvider>
          <ColorProvider>
            <App />
          </ColorProvider>
        </AIProvider>
      </ProductProvider>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
