import axios from 'axios'
import { createContext, useContext, useEffect, useMemo, useState } from 'react';
import { base_url } from "../config"
import useInterval from '../hooks/useInterval';
const Context = createContext([])
const request_endpoint = base_url + "/api/request_ai"
const check_endpoint = base_url + "/api/check_ai/"

const checkAi = (setLoading, setStatusMessage, setResult, setError, task_id) => {
    axios.get(check_endpoint + task_id).then((result) => {
        result = result.data
        if(result.result) {
            //alert("완료!")
            setLoading(false)
            setStatusMessage('사진합성이 완료되었습니다.')            
            setResult(result.result)
            return
        }else {
            setStatusMessage(JSON.stringify(result))
            setTimeout(() => {checkAi(setLoading, setStatusMessage, setResult, setError, task_id)}, 1000)
            //useInterval(() => {checkAi(task_id)}, 1000)
        }
    })
    .catch(e => {
        console.log(e)
        setError(e)
        setLoading(false)
        alert("알 수 없는 에러가 발생하였습니다.")
    })
}

export function AIProvider({children}) {

    const [loading, setLoading] = useState(false)
    const [statusMessage, setStatusMessage] = useState('사진합성을 준비중입니다')
    const [result, setResult] = useState('')
    const [error, setError] = useState('')
    const init = () => {
        setLoading(false)
        setStatusMessage('사진합성을 준비중입니다')
        setResult('')
        setError('')
    }
    const execute = (requestType, modelImage, param) => {
        setLoading(true)
        setStatusMessage('사진합성을 준비중입니다')
        setResult('')
        setError('')
        let get_url = `${request_endpoint}?request_type=${requestType}&model_id=${modelImage}&param=${param}`
        axios.get(get_url).then((r) => {
            r = r.data
            if(r.result)
            {
                setLoading(false)
                setStatusMessage('사진합성이 완료되었습니다.')
                setResult(r.result)
                //alert("완료!!")
            }else{
                //alert(JSON.stringify(r))
                setStatusMessage(JSON.stringify(r))
                checkAi(setLoading, setStatusMessage, setResult, setError, r.task_id)
            }
        }).catch(e => {
            setError(e)
            setLoading(false)
            alert("알 수 없는 에러가 발생하였습니다.")
        })

        return {loading, statusMessage, result, error} 
    }
    
    const context = useMemo(() => { const ret = {
        execute,
        init,
        loading,
        statusMessage,
        result,
        error
    }
    console.log('updated!', ret)
    return ret
}, [
        execute, init, loading, statusMessage, result, error
    ])

    return (
        <Context.Provider value={context}>
            {children}
        </Context.Provider>
    )
}

export function useAI() {
    return useContext(Context);
}

export default {
    useAI, AIProvider
}