import request from "graphql-request";
import { createContext, useEffect, useMemo, useState } from "react";
import { query_url } from "../config";

const defaultModelQuery = `
query{
    allModelFiles {
      modelType,
      file,
      uuid
    }
  }
`


const ModelContext = createContext({
    models = []
})

function ModelProvider({children}) {
    const [models, setModels] = useState([])
    useEffect(() => {
        request(query_url, defaultModelQuery).then((d) => {
            setModels(d.allModelFiles)
        })
    }, [])
    const value = useMemo(() => {
        models: models
    },
    [models])

    return (
        <ModelContext.Provider value={value}>
            {children}
        </ModelContext.Provider>
    )
}

export {ModelContext, ModelProvider}