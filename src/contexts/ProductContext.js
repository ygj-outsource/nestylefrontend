import axios from 'axios'
import { createContext, useContext, useEffect, useState } from 'react';
import { base_url } from '../config';
import useQuery from '../hooks/useQuery';
const Context = createContext([])
function mapToArray(m) {
    return Object.assign({}, ...(m).map(i => (
        {[i.id]: i}
    )))
}
axios.defaults.baseURL = base_url + "/api/"
const categoryQuery = `
query {
    allCategory
    {
      id
      name
    }
  }
`
const brandQuery = `
query {
    allBrand
    {
      id
      name
    }
  }
`
export function ProductProvider({children}) {

    var category = []
    var brand = []
    const [cart, setCart] = useState({})
    const categoryState = useQuery(categoryQuery, {cache:true})
    const brandState = useQuery(brandQuery, {cache:true})

    // 장바구니 새로고침시 서버 또는 로컬에 저장

    if(!categoryState.loading)
        category = (categoryState.data.allCategory)

    if(!brandState.loading)
        brand = mapToArray(brandState.data.allBrand)

    // 카테고리 내 의상 목록
    const getProducts = (category_id, offset=0, size=8) => {
        return new Promise((resolve, reject) => {
            axios.get("/product/list/" + category_id + "?offset=" + offset + "&size=" + size)
            .then(({data}) => {
                resolve(data.data)
            })
        })
    }


    // 브랜드 내 의상 목록
    const getProductsByBrand = (brand_id, offset=0, size=8) => {
        return new Promise((resolve, reject) => {
            axios.get("/product/brand/" + brand_id + "?offset=" + offset + "&size=" + size)
            .then(({data}) => {
                resolve(data.data)
            })
        })
    }

    // 장바구니에 추가
    const pick = (item) => {
        const new_item = {}
        new_item[item.id] = item
        var newCart = {...cart, ...new_item}
        setCart(newCart)
    }

    // 장바구니 제거
    const unpick = (item) => {
        var newCart = JSON.parse(JSON.stringify(cart))
        delete newCart[item.id]
        setCart(newCart)
    }

    const context = {category, cart, brand, getProducts, getProductsByBrand, pick, unpick}

    return (
        <Context.Provider value={context}>
            {children}
        </Context.Provider>
    )
}

export function useProduct() {
    return useContext(Context);
}

export default {
    ProductProvider, useProduct
}