import request from "graphql-request";
import { createContext, useEffect, useMemo, useState } from "react";
import { query_url } from "../config";

const colorQuery = `
query {
    allColor {
      name
      hexcode
      id
    }
  }
`

const ColorContext = createContext({
    colors: []
})


function ColorProvider({children}) {
    const [colors, setColors] = useState([])
    useEffect(() => {
        request(query_url, colorQuery).then((d) => {
            console.log("colors : ", d)
            setColors(d.allColor)
        })
    
    }, [])
    console.log("colors from useMemo : ", colors)

    const value = useMemo(() => ({colors}), [colors, setColors])
    return (
        <ColorContext.Provider value={value}>
            {children}
        </ColorContext.Provider>
    )
}

export {ColorContext, ColorProvider}