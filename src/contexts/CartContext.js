import { createContext, useMemo, useState } from "react"


const getCart = (item) => {
    return fromLocalStorage()
}
const clearCart = (item) => {
    return toLocalStorage([])
}    
const fromLocalStorage = () => {
    var cart = window.localStorage.getItem("cart")
    if(cart == null) cart = "[]"
    if(cart === "{}") cart = "[]" // 옛날 카트 방지용
    cart = JSON.parse(cart)
    return cart
}
const toLocalStorage = (c) => {
    window.localStorage.setItem("cart", JSON.stringify(c))
    console.log("new cart : ", c)
}

const CartContext = createContext({
    cart: fromLocalStorage(),
    addCart: () => {},
    removeCart: () => {},
    getCart: () => {},
    clearCart: () => {}
})
function CartProvider({children})
{
    const [cart, setCart] = useState(fromLocalStorage())
    console.log("initial cart : ", cart)

    const addCart = (item) => {
        var cart = fromLocalStorage()
        cart.push(item)
        toLocalStorage(cart)
        console.log("[addCart] new cart : ", fromLocalStorage(), cart)
        setCart(cart)
    }
    const removeCart = (item) => {
        var cart = fromLocalStorage()
        cart = cart.filter(i => i.id != item.id)
        toLocalStorage(cart)
        console.log("[removeCart] new cart : ", fromLocalStorage())
        setCart(cart)
    }
    
    const value = useMemo(() => ({
        cart: cart,
        addCart: addCart,
        removeCart: removeCart,
        getCart: getCart,
        clearCart: clearCart
    }), [
        cart, addCart, removeCart, getCart, clearCart
    ])
    return (
        <CartContext.Provider value={value}>
            {children}
        </CartContext.Provider>
    )
}

export { CartContext, CartProvider }