
FROM node:12.2.0-alpine
WORKDIR /app
RUN npm install yarn
ENV PATH /app/node_modules/.bin:$PATH

COPY public /app/public
COPY src /app/src
COPY yarn.lock /app/yarn.lock
COPY package.json /app/package.json

RUN yarn install

# 앱 실행
CMD ["yarn", "start"]